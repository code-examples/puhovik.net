# [puhovik.net](https://puhovik.net) source codes

<br/>

### Run puhovik.net on localhost

    # vi /etc/systemd/system/puhovik.net.service

Insert code from puhovik.net.service

    # systemctl enable puhovik.net.service
    # systemctl start puhovik.net.service
    # systemctl status puhovik.net.service

http://localhost:4005
