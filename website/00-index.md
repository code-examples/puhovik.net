---
layout: page
title: Пуховик
permalink: /
---

# Добро пожаловать на сайт Пуховик!


Привет! 

Предлагаю на данном домене разместить информацию по продаже пуховиков. Представить ассортимент имеющихся товаров. 

Если ваша компания как раз занимается такого рода деятельностью, то возможно вам было бы интересно сотрудничать со мной. 

Сам я <a href="//programmist.net">Программист</a>. Оцениваю свои трудозатраты на 20 000 руб. в месяц. Мне кажется, что это не очень много за столь запоминающееся название сайта, которое можно и на визитке написать и на стенде. 


<br/>

С наступающим новым годом, пусть медведев держится сам, здоровья и хорошего настроения!


<br/>

![Пуховик](/website/img/puhovik.jpeg){: .center-image }

<br/>
<br/>

Также имеются домены <strong>palto.org, kurtka.org</strong>. Если захотите сотрудничать по какому-либо из направлений, дайте знать.

<br/>
<br/>

**Мои контакты:**


<img src="http://img.fotografii.org/a3333333mail.gif" alt="Marley" title="Marley"/>